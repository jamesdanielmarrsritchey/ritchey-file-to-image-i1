<?php
#Name:File To Image v1
#Description:Convert a file to an image file. Returns "TRUE" on success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values. Image will be saved as PNG.
#Arguments:'file' (required) is a file path for the file to convert. 'destination' is a file path for where to save the image file to. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):file:file:required,$destination:path:required,display_errors:bool:optional
#Content:
if (function_exists('file_to_image_v1') === FALSE){
function file_to_image_v1($file, $destination, $display_errors = NULL){
	$errors = array();
	$progress = '';
	##Arguments
	if (@is_file($file) === FALSE){
		$errors[] = 'file';
	}
	if (@is_dir(dirname($destination)) === FALSE){
		$errors[] = 'destination';
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	}
	if ($display_errors === TRUE OR $display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task [Convert file data to Ritchey Base Number. Treat 9 byte chunks as decimal RGB color codes, and use to create an image.]
	if (@empty($errors) === TRUE){
		###Determine image resolution
		$file_size = @filesize($file);
		$pixel_quantity = $file_size / 9;
		$pixel_quantity = $pixel_quantity * 4;
		$x = @ceil(sqrt($pixel_quantity));
		$y = $x;
		###Create image (pixel colors will be changed later to add in data)
		$current_x = 0;
		$current_y = 0;
		####Set image background colour to black so that extra pixels will automatically be black.
		$image = @imagecreatetruecolor($x, $y);
		$colour = @imagecolorallocate($image, 0, 0, 0);
		@imagefill($image, 0, 0, $colour);
		###Convert file to Ritchey Base Number (process file in chunks to be more memory friendly. Of course it doesn't matter since image is stored in memory while being created.)
		$location = realpath(dirname(__FILE__));
		require_once $location . '/dependencies/data_to_ritchey_base_number_d_v1.php';
		$handle = fopen($file, "rb");
		while (!feof($handle)) {
			####Read 9 bytes from file (this equates to 36 Ritchey Base Number D digits which can be processed as four 9 digit color codes [eg: 4 pixels]). The last section may not be 9 bytes so when the data is processed as color codes there may be a partial color code that needs to be padded with '0' (when decoding any 3 digit set not between 100-164 is treated as meaning to discard all data from there onward)
			$data = @fread($handle, 9);
			$data = @data_to_ritchey_base_number_d_v1($data, FALSE);
			$data = @str_split($data, 9);
			####Set data pixel colours
			foreach ($data as $value) {
				$colour = @str_split($value, 3);
				#####If there isn't enough data their may not be 3 pieces. Check, and create pieces as '0' if needed.
				if (@isset($colour[1]) === FALSE){
					$colour[1] = 0;
				}
				if (@isset($colour[2]) === FALSE){
					$colour[2] = 0;
				}
				$colour = @imagecolorallocate($image, $colour[0], $colour[1], $colour[2]);
				@imagesetpixel($image, $current_x, $current_y, $colour);
				####Increment current_x so that data is assigned horizontally across x-axis until it reaches the end, and then reset to zero and increment current_y so data is on the next x-axis below it.
				$current_x++;
				if ($current_x >= $x){
					$current_x = 0;
					$current_y++;
				}
			}
		}
		fclose($handle);
		###Save image to PNG file
		@imagepng($image, $destination);
		@imagedestroy($image);
	}
	result:
	##Display Errors
	if ($display_errors === TRUE and @empty($errors === FALSE)){
		$message = @implode(", ", $errors);
		if (function_exists('file_to_image_v1_format_error') === FALSE){
			function file_to_image_v1_format_error($errno, $errstr){
				echo $errstr;
			}
		}
		set_error_handler("file_to_image_v1_format_error");
		trigger_error($message, E_USER_ERROR);
	}
	##Return
	if (@empty($errors) === TRUE){
		return TRUE;
	} else {
		return FALSE;
	}
}
}
?>
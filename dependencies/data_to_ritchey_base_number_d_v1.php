<?php
#Name:Data To Ritchey Base Number D v1
#Description:Convert a data to a number using the Ritchey Base Number D encoding scheme. Returns the number as a string on success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values.
#Arguments:'source' (required) is a path to the file to convert. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):string:string:required,display_errors:bool:optional
#Content:
if (function_exists('data_to_ritchey_base_number_d_v1') === FALSE){
function data_to_ritchey_base_number_d_v1($string, $display_errors = NULL){
	$errors = array();
	$progress = '';
	##Arguments
	if (@empty($string) === TRUE){
		$errors[] = "string";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	}
	if ($display_errors === TRUE OR $display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task [Convert string to Base64. Break Base64 string into an array of single characters. Represent each letter as an incrementing three digit number starting from 100 and going to 164. Convert array to string. NOTE: Base64 can encode be done in chunks of 3 bytes and still achieve the same result as processing a data all at once, but this implementation does not process data 3 bytes at a time. There is no point since it is returning the number, not saving it to another file.]
	if (@empty($errors) === TRUE){
		###Convert to base64
		$string = @base64_encode($string);
		###Convert string to array of single characters
		$string = @str_split($string, 1);
		###Convert each character in the array to a number between 100 and 164.
		foreach ($string as &$character){
			if ($character === "A"){
				$character = '100';
			} else if ($character === "B"){
				$character = '101';
			} else if ($character === "C"){
				$character = '102';
			} else if ($character === "D"){
				$character = '103';
			} else if ($character === "E"){
				$character = '104';
			} else if ($character === "F"){
				$character = '105';
			} else if ($character === "G"){
				$character = '106';
			} else if ($character === "H"){
				$character = '107';
			} else if ($character === "I"){
				$character = '108';	
			} else if ($character === "J"){
				$character = '109';	
			} else if ($character === "K"){
				$character = '110';	
			} else if ($character === "L"){
				$character = '111';	
			} else if ($character === "M"){
				$character = '112';	
			} else if ($character === "N"){
				$character = '113';	
			} else if ($character === "O"){
				$character = '114';	
			} else if ($character === "P"){
				$character = '115';	
			} else if ($character === "Q"){
				$character = '116';	
			} else if ($character === "R"){
				$character = '117';	
			} else if ($character === "S"){
				$character = '118';	
			} else if ($character === "T"){
				$character = '119';	
			} else if ($character === "U"){
				$character = '120';	
			} else if ($character === "V"){
				$character = '121';	
			} else if ($character === "W"){
				$character = '122';	
			} else if ($character === "X"){
				$character = '123';	
			} else if ($character === "Y"){
				$character = '124';	
			} else if ($character === "Z"){
				$character = '125';	
			} else if ($character === "a"){
				$character = '126';	
			} else if ($character === "b"){
				$character = '127';
			} else if ($character === "c"){
				$character = '128';
			} else if ($character === "d"){
				$character = '129';
			} else if ($character === "e"){
				$character = '130';
			} else if ($character === "f"){
				$character = '131';
			} else if ($character === "g"){
				$character = '132';
			} else if ($character === "h"){
				$character = '133';
			} else if ($character === "i"){
				$character = '134';	
			} else if ($character === "j"){
				$character = '135';	
			} else if ($character === "k"){
				$character = '136';	
			} else if ($character === "l"){
				$character = '137';	
			} else if ($character === "m"){
				$character = '138';	
			} else if ($character === "n"){
				$character = '139';	
			} else if ($character === "o"){
				$character = '140';	
			} else if ($character === "p"){
				$character = '141';	
			} else if ($character === "q"){
				$character = '142';	
			} else if ($character === "r"){
				$character = '143';	
			} else if ($character === "s"){
				$character = '144';	
			} else if ($character === "t"){
				$character = '145';	
			} else if ($character === "u"){
				$character = '146';	
			} else if ($character === "v"){
				$character = '147';	
			} else if ($character === "w"){
				$character = '148';	
			} else if ($character === "x"){
				$character = '149';	
			} else if ($character === "y"){
				$character = '150';	
			} else if ($character === "z"){
				$character = '151';	
			} else if ($character === "0"){
				$character = '152';	
			} else if ($character === "1"){
				$character = '153';	
			} else if ($character === "2"){
				$character = '154';	
			} else if ($character === "3"){
				$character = '155';	
			} else if ($character === "4"){
				$character = '156';	
			} else if ($character === "5"){
				$character = '157';	
			} else if ($character === "6"){
				$character = '158';	
			} else if ($character === "7"){
				$character = '159';	
			} else if ($character === "8"){
				$character = '160';	
			} else if ($character === "9"){
				$character = '161';	
			} else if ($character === "+"){
				$character = '162';	
			} else if ($character === "/"){
				$character = '163';	
			} else if ($character === "="){
				$character = '164';	
			} else {
				$errors[] = "task - Convert characters to numbers";
				goto result;
			}
		}
		###Convert array back to string.
		$string = @implode($string);
	}
	result:
	##Display Errors
	if ($display_errors === TRUE and @empty($errors === FALSE)){
		$message = @implode(", ", $errors);
		if (function_exists('data_to_ritchey_base_number_d_v1_format_error') === FALSE){
			function data_to_ritchey_base_number_d_v1_format_error($errno, $errstr){
				echo $errstr;
			}
		}
		set_error_handler("data_to_ritchey_base_number_d_v1_format_error");
		trigger_error($message, E_USER_ERROR);
	}
	##Return
	if (@empty($errors) === TRUE){
		return $string;
	} else {
		return FALSE;
	}
}
}
?>